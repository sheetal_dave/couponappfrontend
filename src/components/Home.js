import React, {useEffect, useState} from 'react';

import {Spinner, Row} from 'react-bootstrap';
import axios from 'axios';

import CouponDetail from "./CouponDetail";

const Home = () => {
    const [coupons, setCoupons] = useState([]);

    useEffect(() => {
        axios.get('coupons')
            .then((res) => {
                setCoupons(res.data.data);
            })
    }, [coupons]);


    return (
        <div className="container">
            <Row>
                {
                    !coupons.length ? <Spinner animation="border" variant="primary"/> :
                        (coupons.map(coupon => {
                            return (
                                <CouponDetail key={coupon.id} coupon={coupon}/>
                            )
                        }))
                }

            </Row>
        </div>
    )
}
export default Home;
