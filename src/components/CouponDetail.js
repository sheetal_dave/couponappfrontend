import React, {useState} from "react";

import {Card, Button, Spinner, Col, Row} from 'react-bootstrap';
import axios from "axios";

const CouponDetail = ({coupon}) => {

    const [loading, setLoading] = useState({});

    const unlockCoupon = (id) => {
        setLoading({id: id, isLoading: true});
        axios.put(`unlock-coupon/${id}`)
            .then(() => {
                setLoading({id: id, isLoading: false});
            });
    }

    return (
        <Row>
            <div className="p-5 text-center">
                <Col xs={3}>
                    <Card style={{width: '30rem'}}>
                        <Card.Img className="rounded" variant="top" src={coupon.logo} width="100px" height="250px"/>
                        <Card.Body>
                            <Card.Title>{coupon.title}</Card.Title>
                            <Card.Text>
                                {coupon.description}
                            </Card.Text>
                            <Button variant="primary" disabled={coupon.unlock}
                                    onClick={() => unlockCoupon(coupon.id)}>
                                {coupon.unlock ? coupon.code : 'UNLOCK'}
                                {loading.id === coupon.id && loading.isLoading &&
                                <Spinner animation="border" variant="info"/>}
                            </Button>
                        </Card.Body>
                    </Card>
                </Col>
            </div>
        </Row>
    )
}
export default CouponDetail;