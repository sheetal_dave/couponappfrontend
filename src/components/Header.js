import React from 'react';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {Navbar, Nav} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from "./Home";

const Header = () => {
    return (
        <Router>
            <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
                <Navbar.Brand> Coupon Management System</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link href="/">Home</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
            <Switch>
                <Route exact path="/"> <Home/> </Route>
            </Switch>
        </Router>
    );
}

export default Header;